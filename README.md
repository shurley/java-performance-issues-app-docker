# Java Performance Issues Docker Demo

This image demonstrates deadlock, gridlock, memory leaks, excessive garbage collection and misconfigured thread pool.

## Getting Started

These instructions will get you a container running this image on your local machine.

### Prerequisites

* Docker
* Git

### Installing from Git

Navigate to the directory you would like to store the project through your shell and execute the following comm
command:

```
git clone https://:@gitlab.cern.ch:8443/shurley/java-performance-issues-app-docker.git
```

### Running the Container Using the Git Project

From with the directory of the project, build the container using this command:

```
sudo docker build -t java_perf_issues .
```

To run the container, execute this command, using the -p args to bind the container ports to external ones and the -e args to set the username, password and permissions as environment variables:

```
sudo docker run -t -p java_perf_issues \
-p 8112:8080 -p 9011:9011 \
-e JMX_REMOTE_PASSWORD='password' \
-e JMX_REMOTE_USERNAME='username' \
-e JMX_REMOTE_PERMISSION='readwrite' java_perf_issues
```

The -p arguments specify which container ports (the second port)are to be bound to which host ports (first port) e.g. 8112(host):8080(container). For the port that RMI uses, please bind it to the same host port e.g. 9011:9011 works but 9112:9011 won't.

The -e arguments specify environment variables. The variables specified here are for the username and password you will use to access the JVM through Java Mission Control. The JMX_REMOTE_PERMISSION variable specifies whether you can issues commands to the JVM or not. 'readwrite' allows you to issues commands, 'readonly' does not. Any other values will result in an error.

Finally, in your browser go to: scottsvm.cern.ch:8112/java_perf_issues-0.0.1-SNAPSHOT

### Running the Container Using the Repo Image

The more convenient way to launch a container with this image is to pull it directly from from its repository and run it. To do this, enter the following into your command prompt:

```
sudo docker run -t -p 8112:8080 -p 9011:9011 \
-e JMX_REMOTE_PASSWORD='password' \
-e JMX_REMOTE_USERNAME='username' \
-e JMX_REMOTE_PERMISSION='readwrite' \
gitlab-registry.cern.ch/shurley/java-performance-issues-app-docker/java_perf_issues:latest
```

### What the Dockerfile Does

The dockerfile creates the necessary directories for the files it will use, updates all its packages and installs gettext. It fetches the tsnnames.ora file from CERN web services and copies the files it needs from the host, placing them in their corresponding directories and exposes the ports tomcat will use. Finally, it makes the custom start script runnable, then runs it, injecting the user-specified log in details into the relevant files.
