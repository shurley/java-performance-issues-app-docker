FROM gitlab-registry.cern.ch/db/k8s-tomcat/tomcat:latest

COPY java_perf_issues-0.0.1-SNAPSHOT.war webapps/ 

RUN mkdir -p /gubbins && \
    yum update -y && \
    yum install -y wget \
    gettext && \
    wget -o /gubbins/tsnnames.ora \
    https://service-oracle-tnsnames.web.cern.ch/service-oracle-tnsnames/tnsnames.ora

COPY certs/jfrremoteKeyStore /gubbins/jfrremoteKeyStore

COPY scripts/start.sh /start.sh
RUN ["chmod", "+x", "/start.sh"]

COPY config/jmxremote.access \
     config/jmxremote.password \
     config/management.properties \
     $JAVA_HOME/lib/management/

COPY config/setenv.sh bin/

EXPOSE 9011 8080

CMD ["/start.sh"]
