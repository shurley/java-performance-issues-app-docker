#!/bin/bash

envsubst '${JMX_REMOTE_USERNAME} ${JMX_REMOTE_PERMISSION}' < $JAVA_HOME/lib/management/jmxremote.access > $JAVA_HOME/lib/management/jmxremote.access

envsubst '${JMX_REMOTE_USERNAME} ${JMX_REMOTE_PASSWORD}' < $JAVA_HOME/lib/management/jmxremote.password > $JAVA_HOME/lib/management/jmxremote.password

$CATALINA_HOME/bin/catalina.sh run
